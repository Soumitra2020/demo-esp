*** Settings ***
Documentation    Suite description
Library  SeleniumLibrary
Library    String

*** Variables ***

*** Test Cases ***
This is demo test case to open and close browser
    Open Salesforce Application
    Close Salesforce Application


*** Keywords ***
Open Salesforce Application
     open browser   https://test.salesforce.com     chrome
     maximize browser window

Close Salesforce Application
     close browser






